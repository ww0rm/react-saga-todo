// @flow
import { actionTypes } from '../reducers/item';

export const load = () => ({
    type: actionTypes.LOAD_ITEMS_REQUEST,
});

export const setStarred = (id, starred) => ({
    type: actionTypes.STAR_ITEM_REQUEST,
    payload: {id, starred}
});

export const setDone = (id, done) => ({
    type: actionTypes.DONE_ITEM_REQUEST,
    payload: {id, done}
});

export const deleteItem = (id) => ({
    type: actionTypes.DELETE_ITEM_REQUEST,
    payload: {id}
});

export const editItem = (id) => ({
    type: actionTypes.EDIT_ITEM_REQUEST,
    payload: {id}
});

export const saveItem = (id, title) => ({
    type: actionTypes.EDIT_ITEM_SUCCESS,
    payload: {id, title}
});

export const editTitle = () => ({
    type: actionTypes.EDIT_TITLE_REQUEST
});

export const saveTitle = (title) => ({
    type: actionTypes.EDIT_TITLE_SUCCESS,
    payload: {title}
});