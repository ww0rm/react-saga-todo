import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/user';

import { withStyles } from '@material-ui/core/styles';
import styles from './styles'

import CenteredBlock from '../../components/CenteredBlock';
import Error from '../../components/Error';

import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

@connect(state => ({user: state.user}), dispatch => ({
    actions: bindActionCreators(actions, dispatch)
}))
@withStyles(styles)
class Auth extends Component {
    constructor () {
        super();
        this.state = {login: '', password: '', errorOpen: false};
    };

    componentWillReceiveProps = (props) => {
        this.setState({...this.state, errorOpen: props.user.errorMessage !== null});
    };

    login = () => {
        this.props.actions.login({login: this.state.login, password: this.state.password});
    };

    handleLoginChange = (e) => {
        this.setState({...this.state, login: e.target.value});
    };

    handlePasswordChange = (e) => {
        this.setState({...this.state, password: e.target.value});
    };

    handleErrorClose = () => {
        this.setState({...this.state, errorOpen: false});
    };

    logout = () => {
        this.props.actions.logout();
    };

    render() {
        const { classes } = this.props;
        return (
            <CenteredBlock title='Login' xs={4}>

                <FormControl className={classes.margin} fullWidth={true}>
                    <TextField
                        label="Login"
                        autoFocus={true}
                        onChange={this.handleLoginChange} />
                </FormControl>

                <FormControl className={classes.margin} fullWidth={true}>
                    <TextField
                        label="Password"
                        type="password"
                        onChange={this.handlePasswordChange} />
                </FormControl>

                <Button variant="contained" color="primary" onClick={this.login} className={classes.button} disabled={this.props.user.loading}>
                    {this.props.user.loading ? (<CircularProgress size={24} className={classes.buttonProgress} />) : 'Log in'}
                </Button>

                <div className={classes.clearfix}/>

                <Error open={this.state.errorOpen} message={this.props.user.errorMessage} handleErrorClose={this.handleErrorClose} />
            </CenteredBlock>
        );
    }
};

export default Auth;
