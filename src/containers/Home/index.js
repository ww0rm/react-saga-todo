import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators  } from 'redux';
import { createSelector } from 'reselect';
import * as itemActions from '../../actions/item';

import {listSelector} from '../../selectors/item';

import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

import CenteredBlock from '../../components/CenteredBlock';
import List from '../../components/ItemList';
import EditModal from '../../components/EditModal';

@connect(createSelector(
    state => ({item: state.item}),
    listSelector,
), dispatch => ({
    itemActions: bindActionCreators(itemActions, dispatch),
}))
@withStyles(styles)
class Home extends Component {
    componentDidMount () {
        if (0 === this.props.item.list.length) {
            this.props.itemActions.load();
        }
    };

    handleReload = () => {
        this.props.itemActions.load();
    };

    saveItem = (title) => {
        this.props.itemActions.saveItem(this.props.item.edit.id, title);
    };

    handleEdit = () => {
        this.props.itemActions.editTitle();
    };

    saveTitle = (title) => {
        this.props.itemActions.saveTitle(title);
    };

    render() {
        const { classes } = this.props;
        console.log(this.props);

        return (
            <div className={classes.root}>
                <CenteredBlock title={this.props.item.title} edit={true} handleEdit={this.handleEdit} xs={8}>
                    <List
                        items={this.props.list || []}
                        loading={this.props.item.loading}
                        actions={{...this.props.itemActions, handleReload: this.handleReload}} />
                </CenteredBlock>

                <EditModal
                    open={this.props.item.edit !== null}
                    value={this.props.item.edit !== null ? this.props.item.edit.title : ''}
                    save={this.saveItem} />

                <EditModal
                    open={this.props.item.editTitle || false}
                    value={this.props.item.title}
                    save={this.saveTitle} />
            </div>
        );
    }
}

export default Home;
