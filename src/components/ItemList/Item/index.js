import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import StarIcon from '@material-ui/icons/Star';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

@withStyles(styles)
class Item extends Component {
    handleToggle = () => {
        this.props.onToogle(this.props.item.id, !this.props.item.done);
    };

    handleStarred = () => {
        this.props.onStarred(this.props.item.id, !this.props.item.starred);
    };

    handleDelete = () => {
        this.props.onDelete(this.props.item.id);
    };

    handleEdit = () => {
        this.props.onEdit(this.props.item.id);
    };

    render() {
        const { classes, item } = this.props;

        return (
                <ListItem
                    key={this.props.key}
                    dense
                    divider={this.props.divider}
                    className={classes.listItem}>
                        <Checkbox
                            checked={item.done}
                            onClick={this.handleToggle}
                            tabIndex={-1} />
                        <ListItemText primary={item.done ? '' : item.title} className={item.done ? classes.textDone : ''} secondary={item.done ? item.title : ''}/>
                        <ListItemSecondaryAction>
                            <IconButton aria-label="Comments" onClick={this.handleStarred}>
                                <StarIcon color={item.starred ? 'primary' : 'inherit'} />
                            </IconButton>
                            <IconButton aria-label="Edit" onClick={this.handleEdit}>
                                <EditIcon />
                            </IconButton>
                            <IconButton aria-label="Delete" onClick={this.handleDelete}>
                                <DeleteIcon />
                            </IconButton>
                        </ListItemSecondaryAction>
                </ListItem>
        );
    }
}

export default Item;
