import grey from '@material-ui/core/colors/grey';

export default theme => ({
    paper: {
        height: '100%',
        color: theme.palette.text.secondary,
        width: '100%'
    },
    textDone: {
        'text-decoration': 'line-through'
    },
    buttonProgress: {
        color: grey[1000],
        marginTop: '10px'
    },
    list: {
        overflow: 'hidden',
    },
    button: {
        marginTop: '15px;'
    },
});