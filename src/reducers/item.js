// @flow
import Immutable from 'seamless-immutable';
import { createReducer } from '../utils';

export const actionTypes = {
    LOAD_ITEMS_REQUEST: 'ITEM/LOAD_ITEMS_REQUEST',
    LOAD_ITEMS_SUCCESS: 'ITEM/LOAD_ITEMS_SUCCESS',
    LOAD_ITEMS_FAIL: 'ITEM/LOAD_ITEMS_FAIL',

    STAR_ITEM_REQUEST: 'ITEM/STAR_ITEM_REQUEST',
    STAR_ITEM_SUCCESS: 'ITEM/STAR_ITEM_SUCCESS',
    STAR_ITEM_FAIL: 'ITEM/STAR_ITEM_FAIL',

    DONE_ITEM_REQUEST: 'ITEM/DONE_ITEM_REQUEST',
    DONE_ITEM_SUCCESS: 'ITEM/DONE_ITEM_SUCCESS',
    DONE_ITEM_FAIL: 'ITEM/DONE_ITEM_FAIL',

    DELETE_ITEM_REQUEST: 'ITEM/DELETE_ITEM_REQUEST',
    DELETE_ITEM_SUCCESS: 'ITEM/DELETE_ITEM_SUCCESS',
    DELETE_ITEM_FAIL: 'ITEM/DELETE_ITEM_FAIL',

    EDIT_ITEM_REQUEST: 'ITEM/EDIT_ITEM_REQUEST',
    EDIT_ITEM_SUCCESS: 'ITEM/EDIT_ITEM_SUCCESS',
    EDIT_ITEM_FAIL: 'ITEM/EDIT_ITEM_FAIL',

    EDIT_TITLE_REQUEST: 'ITEM/EDIT_TITLE_REQUEST',
    EDIT_TITLE_SUCCESS: 'ITEM/EDIT_TITLE_SUCCESS',
    EDIT_TITLE_FAIL: 'ITEM/EDIT_TITLE_FAIL',

    ADD_ITEM: 'ITEM/ADD_ITEM',
    CHECK_ITEM: 'ITEM/CHECK_ITEM',
    SAVE_ITEM: 'ITEM/SAVE_ITEM',
    STAR_ITEM: 'ITEM/STAR_ITEM',
};

export type itemType = {
    list: [],
    title: string,
    loading: boolean,
    edit: ?{},
    titleEdit: boolean,
    errorMessage: ?string
};

const initialState: itemType = new Immutable({
    list: [],
    title: 'List',
    loading: false,
    edit: null,
    titleEdit: false,
    errorMessage: null
});

export default createReducer(initialState, {
    [actionTypes.LOAD_ITEMS_REQUEST]: (state: itemType): itemType => state.merge({
        loading: true,
    }),

    [actionTypes.LOAD_ITEMS_SUCCESS]: (state: itemType, list: []): itemType => state.merge({
        list,
        loading: false,
    }),

    [actionTypes.LOAD_ITEMS_FAIL]: (state: itemType, errorMessage: string): itemType => state.merge({
        errorMessage
    }),

    [actionTypes.STAR_ITEM_REQUEST]: (state: itemType, item: {}): itemType => state.merge({
        list: state.list.map((v) => {
            if (v.id === item.id) {
                return {...v, starred: item.starred};
            }

            return v;
        }),
    }),

    [actionTypes.DONE_ITEM_REQUEST]: (state: itemType, item: {}): itemType => state.merge({
        list: state.list.map((v) => {
            if (v.id === item.id) {
                return {...v, done: item.done};
            }

            return v;
        }),
    }),

    [actionTypes.DELETE_ITEM_REQUEST]: (state: itemType, item: {}): itemType => state.merge({
        list: state.list.filter((v) => {
            return v.id !== item.id;
        }),
    }),

    [actionTypes.EDIT_ITEM_REQUEST]: (state: itemType, item: {}): itemType => state.merge({
        ...state,
        edit: state.list.filter((i) => {return i.id === item.id})[0]
    }),

    [actionTypes.EDIT_ITEM_SUCCESS]: (state: itemType, item: {}): itemType => state.merge({
        list: state.list.map((v) => {
            if (v.id === item.id) {
                return {...v, title: item.title};
            }

            return v;
        }),
        edit: null
    }),

    [actionTypes.EDIT_TITLE_REQUEST]: (state: itemType): itemType => state.merge({
        editTitle: true
    }),

    [actionTypes.EDIT_TITLE_SUCCESS]: (state: itemType, item: {}): itemType => state.merge({
        title: item.title,
        editTitle: false
    }),
});