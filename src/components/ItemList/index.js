import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

import Item from './Item';

import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import FlipMove from 'react-flip-move';

@withStyles(styles)
class ItemList extends Component {
    render() {
        const { classes } = this.props;

        return (
            this.props.loading ? (
                <div align="center">
                    <CircularProgress size={48} className={classes.buttonProgress} />
                </div>
            ) : (
                this.props.items.length ? (
                    <List className={classes.list} style={{ position: 'relative' }}>
                        <FlipMove
                            staggerDurationBy="30"
                            duration={400}
                            appearAnimation='fade'>
                            {
                                this.props.items.map((item, key) => {
                                    return <Item
                                            key={item.title}
                                            item={item}
                                            divider={this.props.items.length !== key + 1}
                                            onStarred={this.props.actions.setStarred}
                                            onToogle={this.props.actions.setDone}
                                            onDelete={this.props.actions.deleteItem}
                                            onEdit={this.props.actions.editItem} />
                                })
                            }
                        </FlipMove>
                    </List>
                ) : (
                    <div key={0} align="center">
                        <Typography variant="subheading" align="center" color="inherit">Nothing to show</Typography>
                        <Button onClick={this.props.actions.handleReload} variant="contained" color="primary" className={classes.button}>Reload</Button>
                    </div>
                )
            )
        );
    }
}

export default ItemList;
