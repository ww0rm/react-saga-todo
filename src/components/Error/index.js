import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';

@withStyles(styles)
class Error extends Component {
    state = {
        open: false,
    };

    componentWillReceiveProps = (props) => {
        this.setState({open: props.open});
    };

    handleClose = (event, reason) => {
        this.setState({open: false});
        this.props.handleErrorClose();
    };

    render() {
        const { classes } = this.props;

        return (
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                open={this.state.open}
            >
                <SnackbarContent
                    className={classes.error}
                    aria-describedby="client-snackbar"
                    message={
                        <span id="client-snackbar" className={classes.message}>
                          <ErrorIcon className={classes.icon} /> {this.props.message}
                        </span>
                    }
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            className={classes.close}
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                    ]}
                />
            </Snackbar>
        );
    }
}

export default Error;
