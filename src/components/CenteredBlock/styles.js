export default theme => ({
    root: {
        flexGrow: 1,
        height: '100%',
    },
    auth: {
        height: '100%',
    },
    paper: {
        padding: theme.spacing.unit * 2,
        height: '100%',
        color: theme.palette.text.secondary,
    },
    title: {
        marginBottom: '20px',
        marginTop: '20px'
    },
    editButton: {
        float: 'right',
        marginTop: '-15px'
    },
    titleWrapper: {
        position: 'relative'
    }
});