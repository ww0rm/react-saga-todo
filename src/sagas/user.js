import { actionTypes } from '../reducers/user';
import { put, takeLatest, call } from 'redux-saga/effects';
import history from '../config/history';

const users = [
    {
        id: 1,
        firstName: 'Ihor',
        lastName: 'Skliar',
        login: '1',
        password: '2',
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiIxIiwiZW1haWwiOiJza2xpYXIuaWhvckBnbWFpbC5jb20ifQ.oXJL3bpprNbgFMhWdVBGK-ZTww5i_Bb9LCo-Mfz3c_M',
    },
];

const loginMock = ({ login, password }) => new Promise((resolve, reject) => {
    setTimeout(() => {
        const currentUser = users.find(user => user.login === login && user.password === password);
        if (currentUser) {
            return resolve(currentUser);
        }

        return reject(new Error('User not found'));
    }, 1000);
});

export function* login() {
    yield takeLatest(actionTypes.LOGIN_REQUEST, function* doLogin({ payload }) {
        try {
            // const data = yield fetch('https://api.myjson.com/bins/oivjj');
            // const tickets = yield data.json();
            const user = yield loginMock(payload);
            yield put({ type: actionTypes.LOGIN_SUCCESS, payload: user });
            yield call(history.push, '/');
        } catch (e) {
            // eslint-disable-next-line no-console
            console.error(e);
            yield put({ type: actionTypes.LOGIN_FAILURE, payload: e.message });
        }
    });
}

export function* logout() {
    yield takeLatest(actionTypes.LOGOUT, function* doLogout() {
        try {
            yield call(history.push, '/auth');
        } catch (e) {
            // eslint-disable-next-line no-console
            console.error(e);
        }
    });
}
