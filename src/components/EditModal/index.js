import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

@withStyles(styles)
class EditModal extends Component {
    componentWillReceiveProps = (props) => {
        this.setState({title: props.value});
    };

    handleTitleChange = (e) => {
        this.setState({title: e.target.value});
    };

    handleSave = () => {
        this.props.save(this.state.title);
    };

    render() {
        const { classes } = this.props;

        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.props.open}>
                <div className={classes.paperModal}>
                    <FormControl className={classes.margin} fullWidth={true}>
                        <TextField
                            id="title"
                            label="Title"
                            autoFocus={true}
                            defaultValue={this.props.value}
                            onChange={this.handleTitleChange} />
                    </FormControl>
                    <div align="right">
                        <Button onClick={this.handleSave} variant="contained" color="primary" className={classes.button}>Save</Button>
                    </div>
                </div>
            </Modal>
        );
    }
}

export default EditModal;
