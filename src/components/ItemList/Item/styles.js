export default theme => ({
    paper: {
        height: '100%',
        color: theme.palette.text.secondary,
        width: '100%'
    },
    textDone: {
        'text-decoration': 'line-through'
    }
});