import grey from '@material-ui/core/colors/grey';

export default theme => ({
    root: {
        flexGrow: 1,
        height: '100%',
    },
    auth: {
        height: '100%',
    },
    paper: {
        padding: theme.spacing.unit * 2,
        height: '100%',
        color: theme.palette.text.secondary,
    },
    control: {
        padding: theme.spacing.unit * 2,
    },
    margin: {
        margin: theme.spacing.unit,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    button: {
        float: 'right',
        marginTop: '15px;'
    },
    clearfix: {
        clear: 'both',
    },
    buttonProgress: {
        color: grey[1000],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
});