// @flow
import { actionTypes } from '../reducers/user';

type loginType = {
    login: string,
    password: string,
};

export const login = (loginData: loginType) => ({
    payload: loginData,
    type: actionTypes.LOGIN_REQUEST,
});

export const logout = () => ({
    type: actionTypes.LOGOUT,
});