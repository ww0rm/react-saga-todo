import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators  } from 'redux';
import history from '../../config/history'
import {Route, Switch} from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import * as userActions from '../../actions/user';

import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

import Header from '../../components/Header';
import Home from '../Home';
import Auth from '../Auth';

@connect(
    state => ({user: state.user})
, dispatch => ({
    userActions: bindActionCreators(userActions, dispatch),
}))
@withStyles(styles)
class App extends Component {
    componentDidMount () {
        console.log(this.props);
        if (!this.props.user.isLoggedIn && '/auth' !== history.location.pathname) {
            history.replace('/auth');
        }
    };

    handleLogout = () => {
        this.props.userActions.logout();
    };

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Header
                    isLoggedIn={this.props.user.isLoggedIn}
                    handleLogout={this.handleLogout} />

                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route path='/auth' component={Auth}/>
                    </Switch>
                </ConnectedRouter>
            </div>
        );
    }
}

export default App;
