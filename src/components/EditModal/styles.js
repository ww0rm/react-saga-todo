export default theme => ({
    margin: {
        margin: theme.spacing.unit,
    },
    paperModal: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 4,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
    },
    button: {
        marginTop: '15px;'
    },
});