export default theme => ({
    root: {
        flexGrow: 1,
        height: '100%',
    },
    margin: {
        margin: theme.spacing.unit,
    },
    button: {
        marginTop: '15px;'
    },
    paperModal: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 4,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
    },
    editButton: {
        float: 'right',
        marginTop: '-35px'
    }
});