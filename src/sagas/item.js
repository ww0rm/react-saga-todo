import { actionTypes } from '../reducers/item';
import { put, takeLatest } from 'redux-saga/effects';

const items = [
    {
        id: 1,
        title: 'test 1',
        starred: false,
        done: false,
    },
    {
        id: 2,
        title: 'test 2',
        starred: false,
        done: false,
    },
    {
        id: 3,
        title: 'test 3',
        starred: false,
        done: false,
    },
    {
        id: 4,
        title: 'test 4',
        starred: false,
        done: false,
    },
    {
        id: 5,
        title: 'test 5',
        starred: false,
        done: false,
    },
];

const loadMock = () => new Promise((resolve, reject) => {
    setTimeout(() => {
        return resolve(items.map((item) => {
            return item;
        }));

        // return reject(new Error('User not found'));
    }, 300);
});

export function* load() {
    yield takeLatest(actionTypes.LOAD_ITEMS_REQUEST, function* doLoad({ payload }) {
        try {
            // const data = yield fetch('https://api.myjson.com/bins/oivjj');
            // const tickets = yield data.json();
            const items = yield loadMock(payload);
            yield put({ type: actionTypes.LOAD_ITEMS_SUCCESS, payload: items });
        } catch (e) {
            // eslint-disable-next-line no-console
            console.error(e);
            yield put({ type: actionTypes.LOAD_ITEMS_FAIL, payload: e.message });
        }
    });
}
