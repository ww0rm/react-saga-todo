export const listSelector = (state) => {
    let list = state.item.list.asMutable() || [];

    list = list.sort((i1, i2) => {
        return i2.done - i1.done;
    });

    return {
        ...state,
        list
    };
};