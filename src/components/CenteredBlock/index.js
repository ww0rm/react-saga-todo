import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';

@withStyles(styles)
class CenteredBlock extends Component {
    render() {
        const { classes } = this.props;

        return (
            <Grid container className={classes.root}>
                <Grid item xs={12}>
                    <Grid
                        container
                        spacing={40}
                        className={classes.auth}
                        alignItems="center"
                        justify="center">
                        <Grid key="0" item xs={this.props.xs || 6}>
                            <Paper className={classes.paper} xs={8}>
                                {
                                    this.props.title ? (
                                        <div className={classes.titleWrapper}>
                                            {
                                                this.props.edit ? (
                                                    <IconButton className={classes.editButton} aria-label="Edit" onClick={this.props.handleEdit}><EditIcon /></IconButton>
                                                ) : ''
                                            }
                                            <Typography variant="title" gutterBottom align="center" className={classes.title}>{this.props.title}</Typography>
                                        </div>
                                    ) : ''
                                }
                                {this.props.children}
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

export default CenteredBlock;
